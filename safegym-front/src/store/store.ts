import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [
    createPersistedState()
  ],
  state: {
    token: null,
    user: null,
    isAdmin: false,
    isUserLoggedIn: false,
    firstName: null,
    lastName: null
  },
  getters: {
    isUserLoggedIn(state){
      return state.isUserLoggedIn;
    },
    isAdmin(state){
      return state.isAdmin;
    }
  },
  mutations: {
    setToken(state, token){
      state.token = token
      if(token){
        state.isUserLoggedIn = true;
      } else { state.isUserLoggedIn = false }
    },
    setUser(state, user){
      state.user = user
      if(user){
        state.isAdmin = user.data.role == "admin";
        //
        state.firstName = user.data.firstName;
        state.lastName = user.data.lastName;
      }  
    },
    setFirstName(state, firstName){
      state.firstName = firstName;
    },
    setLastName(state, lastName){
      state.lastName = lastName;
    }
  },
  actions: {
    setToken({commit}, token){
      commit('setToken', token)
    },
    setUser({commit}, user){
      commit('setUser', user)
    },
    setFirstName({commit}, firstName:string){
      commit('setFirstName', firstName)
    },
    setLastName({commit}, lastName:string){
      commit('setLastName', lastName)
    }
  },
  modules: {
  }
})
