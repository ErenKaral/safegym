/* eslint-disable */

export const nameRules = [
    v => !!v || "Ce champs est requis",
    v => v.length >= 2  || 'Le nom doit contenir au moins 2 caractères',
    v => v.length <= 30  || 'Maximum 30 caractères',
];

export const durationRules = [
    v => !!v || "Ce champs est requis",
];

export const setsRules = [
    v => v >= 0 && v <= 10 || 'Nombre invalide',
];

export const repetitionsRules = [
    v => v >= 0 && v <= 100 || 'Nombre invalide',
];