/* eslint-disable */

export const nameRules = [
    v => !!v || "Ce champs est requis",
    v => v.length >= 2  || 'Le nom doit contenir au moins 2 caractères',
    v => v.length <= 30  || 'Maximum 30 caractères',
];

export const hourRules = [
  v => !!v || "Ce champs est requis",
];

export const caloriesRules = [
  v => v >= 0 && v <= 1500 || 'Nombre invalide',
];

export const carbsRules = [
  v => v >= 0 && v <= 200 || 'Nombre invalide',
];

export const fatsRules = [
  v => v >= 0 && v <= 100 || 'Nombre invalide',
];

export const proteinsRules = [
  v => v >= 0 && v <= 100 || 'Nombre invalide',
];