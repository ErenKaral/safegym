/* eslint-disable */

export const firstNameRules = [
  v => !!v || "Prénom requis",
  v => v.length >= 2  || 'Votre prénom doit contenir au moins 2 caractères',
  v => v.length <= 20  || 'Maximum 20 caractères',
];

export const lastNameRules = [
  v => !!v || "Nom requis",
  v => v.length >= 2  || 'Votre nom doit contenir au moins 2 caractères',
  v => v.length <= 20  || 'Maximum 20 caractères',
];

export const streetRules = [
  v => !!v || "Rue requis",
  v => v.length >= 2  || 'La rue doit contenir au moins 2 caractères',
  v => v.length <= 30  || 'Maximum 30 caractères',  
];

export const numberRules = [
  v => v > 0 && v <= 999 || 'Nombre invalide',  
];

export const zipcodeRules = [
  v => v > 0 || 'Nombre invalide',  
];

export const cityRules = [
  v => !!v || "Ville requis",
  v => v.length >= 2  || 'La rue doit contenir au moins 2 caractères',
  v => v.length <= 30  || 'Maximum 30 caractères',  
];

export const emailRules = [
  v => !!v || "E-mail requis",
  v => /.+@.+\..+/.test(v) || 'Votre e-mail doit être valide',   
];

export const passwordRules = [
  v => !!v || "Mot de passe requis",
  v => v.length >= 6  || 'Le mot de passe doit contenir au moins 6 caractères',
];
