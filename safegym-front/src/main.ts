import Vue from 'vue'
import App from './App.vue'
import router from '../src/router'
import store from './store/store'
import vuetify from './plugins/vuetify'
import { sync } from 'vuex-router-sync'
import VueGoogleCharts from 'vue-google-charts'

Vue.use(VueGoogleCharts)


Vue.config.productionTip = false

sync(store, router)


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')



