import api from '@/services/Api';

/* eslint-disable */
class GymService {

  async getGym(id: number) : Promise<any> {
    return api.get(`/gym/${id}`).then(gym => {
      return gym.data;
    });
  }

  async updateGym(id: number, data: any) : Promise<any> {
    return api.put(`/gym/${id}`, data).then(gym => {
      return gym.data;
    })
  }

}

export default new GymService();