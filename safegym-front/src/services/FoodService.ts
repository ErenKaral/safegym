import api from '@/services/Api';

/* eslint-disable */
class FoodService {

  async addFood(id: number, data: any): Promise<any> {
    return api.post(`/food/${id}`, data).then(food => {
      return food.data;
    })
  }

  async deleteFood(id: number): Promise<any> {
    return api.delete(`/food/${id}`).then(food => {
      return food.data;
    })
  }

  async updateFood(id: number, data: any) : Promise<any> {
    return api.put(`/food/${id}`, data).then(food => {
      return food.data;
    })
  }


}

export default new FoodService();