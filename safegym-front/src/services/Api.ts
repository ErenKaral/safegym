import axios, { AxiosInstance } from "axios";
import store from '../store/store';

const apiClient: AxiosInstance = axios.create({
  baseURL: "http://localhost:3000/api",
  headers: {
    Authorization: `Bearer ${store.state.token}`,
    "Content-type": "application/json",
  },
});

export default apiClient;