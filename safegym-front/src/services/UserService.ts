import api from '@/services/Api';

/* eslint-disable */
class UserService {

  async createUser(data: any): Promise<any> {
    return api.post("/signup", data).then(user => {
      return user.data;
    });
  }

  async getAll() : Promise<any> {
    return api.get('/users').then(users =>{
      return users.data;
    });
  }

  async getUser(id: number) : Promise<any> {
    return api.get(`/user/${id}`).then(user => {
      return user.data;
    });
  }

  async getUserByRdvDate(date: string) : Promise<any> {
    return api.get(`/users/${date}`).then(user => {
      return user.data;
    });
  }

  async deleteUser(id: number) : Promise<any> {
    return api.delete(`/user/${id}`).then(user => {
      return user.data;
    })
  }

  async updateUser(id: number, data: any) : Promise<any> {
    return api.put(`/user/${id}`, data).then(user => {
      return user.data;
    })
  }

  async sendMail(data: any) : Promise<any> {
    return api.post(`/mail`, data).then(mail => {
      return mail.data;
    })
  }

  async sendPreventionMail(data: any) : Promise<any> {
    return api.post(`/preventionmail`, data).then(mail => {
      return mail.data;
    })
  }

}

export default new UserService();