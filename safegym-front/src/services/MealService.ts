import api from '@/services/Api';

/* eslint-disable */
class MealService {

  async getAll(id: number, date: string): Promise<any> {
    return api.get(`/meal/${id}/${date}`).then(meal => {
      return meal.data;
    });
  }

  async getAllByUserId(id: number): Promise<any> {
    return api.get(`/meal/${id}`).then(meal => {
      return meal.data;
    });
  }

  async makeMeal(data: any): Promise<any> {
    return api.post(`/meal`, data).then(meal => {
      return meal.data;
    })
  }

  async deleteMeal(id: number): Promise<any> {
    return api.delete(`/meal/${id}`).then(meal => {
      return meal.data;
    })
  }

  async updateMeal(id: number, data: any) : Promise<any> {
    return api.put(`/meal/${id}`, data).then(meal => {
      return meal.data;
    })
  }

}

export default new MealService();