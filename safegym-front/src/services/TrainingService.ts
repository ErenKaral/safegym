import api from '@/services/Api';

/* eslint-disable */
class TrainingService {

  async getAll(id: number, date: string): Promise<any> {
    return api.get(`/training/${id}/${date}`).then(training => {
      return training.data;
    });
  }

  async getAllByUserId(id: number): Promise<any> {
    return api.get(`/training/${id}`).then(training => {
      return training.data;
    });
  }

  async makeTraining(data: any): Promise<any> {
    return api.post(`/training`, data).then(training => {
      return training.data;
    })
  }

  async deleteTraining(id: number): Promise<any> {
    return api.delete(`/training/${id}`).then(training => {
      return training.data;
    })
  }

  async updateTraining(id: number, data: any) : Promise<any> {
    return api.put(`/training/${id}`, data).then(training => {
      return training.data;
    })
  }

}

export default new TrainingService();