import api from '@/services/Api';

/* eslint-disable */
class RdvService {

  async makeRdv(data: any): Promise<any> {
    return api.post("/rdv", data);
  }

  async getAll(id: number): Promise<any> {
    return api.get(`/rdvs/${id}`).then(rdv => {
      return rdv.data;
    });
  }

  async delete(id: number): Promise<any> {
    return api.delete(`/rdv/${id}`).then(rdv => {
      return rdv.data;
    });
  }

  async getRdvs(date: any): Promise<any> {
    return api.get(`/rdvsbydate/${date}`).then(rdv => {
      return rdv.data;
    })
  }

}

export default new RdvService();
