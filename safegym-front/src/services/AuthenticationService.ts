import api from '@/services/Api';

/* eslint-disable */
class AuthenticationService {

    login(data: any): Promise<any> {
      return api.post("/login", data);
    }

    signup(data: any): Promise<any> {
      return api.post("/signup", data);
    }
}

export default new AuthenticationService();
