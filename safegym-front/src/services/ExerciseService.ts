import api from '@/services/Api';

/* eslint-disable */
class ExerciseService {

  async addExercise(id: number, data: any): Promise<any> {
    return api.post(`/exercise/${id}`, data).then(exercise => {
      return exercise.data;
    })
  }

  async deleteExercise(id: number): Promise<any> {
    return api.delete(`/exercise/${id}`).then(exercise => {
      return exercise.data;
    })
  }

  async updateExercise(id: number, data: any) : Promise<any> {
    return api.put(`/exercise/${id}`, data).then(exercise => {
      return exercise.data;
    })
  }
  
  
}
  
export default new ExerciseService();