import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import store from '@/store/store'
import Rdv from '../views/Rdv.vue'
import Login from '../views/Login.vue'
import Signup from '../views/Signup.vue'
import Training from '../views/Training.vue'
import Profil from '../views/Profil.vue'
import Members from '../views/Members.vue'
import NotFound from '../views/NotFound.vue'
import Prevention from '../views/Prevention.vue'
import SafeMail from '../views/SafeMail.vue'
import Nutrition from '@/views/Nutrition.vue'


Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Profil,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup,
    meta: {
      admin: false,
      hideNavbar: true,
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      admin: false,
      hideNavbar: true,
    }
  },
  {
    path: '/rdv',
    name: 'Rdv',
    component: Rdv,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/training',
    name: 'Training',
    component: Training,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/profil',
    name: 'Profil',
    component: Profil,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/members',
    name: 'Members',
    component: Members,
    meta: {
      admin: true,
      requiresAuth: true,
    }
  },
  {
    path: '/prevention',
    name: 'Prevention',
    component: Prevention,
    meta: {
      admin: true,
      requiresAuth: true,
    }
  },
  {
    path: '/safemail',
    name: 'SafeMail',
    component: SafeMail,
    meta: {
      admin: true,
      requiresAuth: true,
    }
  },
  {
    path: '/nutrition',
    name: 'Nutrition',
    component: Nutrition,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "*",
    name: "404 NotFound",
    component: NotFound,
    meta: {
      hideNavbar: true,
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});


router.afterEach(to => {
  if (to.name) {
    document.title = to.name + " | Safe GYM";
  }
});


router.beforeEach((to,from,next) => {
  if(to.matched.some(rec => rec.meta.requiresAuth)){
    if(store.getters.isUserLoggedIn){
      next();
    }
    else{
      next({name:'Login'});
    }
  }
  else{
    next();
  }
  if(to.matched.some(rec => rec.meta.admin)){
    if(store.getters.isAdmin){
      next();
    }
    else{
      next({name:'Profil'});
    }
  }
  else{
    next();
  }
});


export default router
