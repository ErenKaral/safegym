import Exercise from './Exercise';

export default interface Training {
    id: number;
    name: string;
    date: string;
    duration: string;
    created: string;
    userId : number;
    Exercises : Exercise[];
}