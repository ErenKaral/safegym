export default interface Food {
    id: number;
    name: string;
    calories: number;
    fats: number;
    carbs: number;
    proteins: number;
    mealId: number;
}