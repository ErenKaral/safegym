export default interface Rdv {
    id: number;
    date: string;
    duration: number;
    created: string;
    userId : number;
}