export default interface ResponseData {
    message: any;
    data: any;
    token: string;
}