import Food from './Food';

export default interface Meal {
    id: number;
    name: string;
    date: string;
    userId : number;
    Food: Food[];
}