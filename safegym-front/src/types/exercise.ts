export default interface Exercise {
    id: number;
    name: string;
    set: number;
    repetition : number;
    trainingId: number;
}