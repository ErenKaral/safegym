export default interface Gym {
    id: number;
    name: string;
    address: string;
    maxPlace: number;
    created : string;
}