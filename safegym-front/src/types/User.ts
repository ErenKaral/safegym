export default interface User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    street: string;
    number: number;
    zipcode: number;
    city: string;
    password: string;
    role : string;
    created : string;
    gymId: number;
}