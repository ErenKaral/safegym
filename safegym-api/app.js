const { Sequelize, DataTypes } = require('sequelize');
const express = require('express');
const morgan = require('morgan');
const favicon = require('serve-favicon');
const users = require('./src/db/mock-user');
const gyms = require('./src/db/mock-gym');
const bcrypt = require('bcrypt');
const db = require('./src/db/dbConfig');
const cors = require('cors');

const app = express();
const port = 3000;

app
  .use(favicon(__dirname + '/favicon.ico'))
  .use(morgan('dev'))
  .use(express.json())
  .use(cors());


const Gym = db.gyms;
const User = db.users;

db.sequelize.sync({force: true}).then(() => {

  gyms.map(gym => {
    Gym.create({
      name: gym.name,
      address: gym.address,
      maxPlace: gym.maxPlace,
      created: gym.created,
    })
  })

  users.map(user => {
    bcrypt.hash(user.password, 10)
    .then(hash => 
      User.create({
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        street: user.street,
        number: user.number,
        zipcode: user.zipcode,
        city: user.city,
        password: hash,
        role: user.role,
        gymId: user.gymId,
      })
    )
  })
  console.log("La base de donnée a bien été initialisée.");
});

app.get('/', (req, res) => res.send('Hello, Safe GYM ! 👋'));

//Points de terminaisons:
require('./src/routes/gymRoute')(app);
require('./src/routes/userRoute')(app);
require('./src/routes/rdvRoute')(app);
require('./src/routes/trainingRoute')(app);
require('./src/routes/exerciseRoute')(app);
require('./src/routes/mealRoute')(app);
require('./src/routes/foodRoute')(app);

// On gère les routes 404.
app.use(({res}) => {
  res.status(404).send('<h1>Impossible de trouver la ressource demandée ! Essayez une autre URL.</h1>');
});

app.listen(port, () => console.log(`L'application est démarrée sur http://localhost:${port}`));
