const auth = require('../auth/auth');
const { validateFood } = require('../middlewares/validators/foodValidator');

module.exports = function(app) {

  const foods = require('../controllers/foodController');

  //Create a new exercise by the given meal id.
  app.post('/api/food/:id', auth, validateFood, foods.create);

  //Delete the food by id.
  app.delete('/api/food/:id', auth, foods.deleteFood);

  //Update the food by id.
  app.put('/api/food/:id', auth, foods.updateFood);

}