const auth = require('../auth/auth');
const { validateUser } = require('../middlewares/validators/userValidator');
const { updateUser } = require('../middlewares/validators/updateUser');

module.exports = function(app) {

    const users = require('../controllers/userController');

    //Login user
    app.post('/api/login', users.login);

    //Create new user
    app.post('/api/signup', validateUser, users.signup);

    //Return the user by id
    app.get('/api/user/:id', auth, users.findByPk);

    //Return all users
    app.get('/api/users', auth, users.findUsers);

    //Return all users with the given Rdv date.
    app.get('/api/users/:date', auth, users.findUsersByDate)

    //Delete the user by id
    app.delete('/api/user/:id', auth, users.deleteUser);

    //Update the user by id
    app.put('/api/user/:id', auth, updateUser, users.updateUser);

    //Send custom mail
    app.post('/api/mail', users.sendCustomMail);

    //Send prevention mail
    app.post('/api/preventionmail', users.sendPreventionMail);

}