const auth = require('../auth/auth');
const { validateMeal } = require('../middlewares/validators/mealValidator');

module.exports = function(app) {

  const meals = require('../controllers/mealController');

  //Create a new meal.
  app.post('/api/meal', auth, validateMeal, meals.create);

  //Return all meals include foods by the given user id and date.
  app.get('/api/meal/:id/:date', auth, meals.findAllByDate);

  //Return all meals include foods by the given user id.
  app.get('/api/meal/:id/', auth, meals.findAllById);

  //Delete the meal by id
  app.delete('/api/meal/:id', auth, meals.deleteMeal);

  //Update the meal by id
  app.put('/api/meal/:id', auth, validateMeal, meals.updateMeal);
  
}