const auth = require('../auth/auth');

module.exports = function(app) {

  const exercises = require('../controllers/exerciseController');

  //Create a new exercise for the given training by id.
  app.post('/api/exercise/:id', exercises.create);

  //Delete the exercise by id.
  app.delete('/api/exercise/:id', auth, exercises.deleteExercise);

  //Update the exercise by id.
  app.put('/api/exercise/:id', auth, exercises.updateExercise);

}