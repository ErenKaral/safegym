const auth = require('../auth/auth');

module.exports = function(app) {

    const rdvs = require('../controllers/rdvController');

    //Create a new rdv
    app.post('/api/rdv', auth, rdvs.create);

    //Return all rdvs by the given user id
    app.get('/api/rdvs/:id', auth, rdvs.findAllByUserPk);

    //Delete the rdv by id
    app.delete('/api/rdv/:id', auth, rdvs.deleteRdv);

    //Get all rdvs by date
    app.get('/api/rdvsbydate/:date', auth, rdvs.findAllByDate);

}