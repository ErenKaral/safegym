const auth = require('../auth/auth');
const { validateGym } = require('../middlewares/validators/gymValidator');

module.exports = function(app) {

  const gyms = require('../controllers/gymController');

  //Return the gym by id
  app.get('/api/gym/:id', auth, gyms.getGym);

  //Update the gym by id
  app.put('/api/gym/:id', auth, validateGym, gyms.updateGym);


}