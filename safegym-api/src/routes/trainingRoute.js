const auth = require('../auth/auth');
const { validateTraining } = require('../middlewares/validators/trainingValidator');


module.exports = function(app) {

  const trainings = require('../controllers/trainingController');

  //Create a new training for the given user by id.
  app.post('/api/training', validateTraining, trainings.create);

  //Return all trainings include exercises by the given user id and date.
  app.get('/api/training/:id/:date', auth, trainings.findAllByDate);

  //Return all trainings include exercises by the given user id.
  app.get('/api/training/:id', auth, trainings.findAllByUserPk);

  //Delete the training by id
  app.delete('/api/training/:id', auth, trainings.deleteTraining);

  //Update the training by id
  app.put('/api/training/:id', auth, validateTraining, trainings.updateTraining);

}