const users = [
  {
    id: 1,
    firstName: "Eren",
    lastName: "Karal",
    email: "erenkaral96@hotmail.com",
    street: "Rue d'Ecosse",
    number: 92,
    zipcode: 1060,
    city: "Saint-Gilles",
    password: "safegym",
    role: "admin",
    created: new Date(),
    gymId: 1
  },
  {
    id: 2,
    firstName: "Boris",
    lastName: "Verhaegen",
    email: "boris@epfc.eu",
    street: "Rue Henri Lambert",
    number: 383,
    zipcode: 1210,
    city: "Saint-Josse-ten-Noode",
    password: "safegym",
    role: "admin",
    created: new Date(),
    gymId: 1
  },
  {
    id: 3,
    firstName: "Luciano",
    lastName: "Botson",
    email: "luciano@hotmail.com",
    street: "Schoolstraat",
    number: 497,
    zipcode: 1000,
    city: "Bruxelles",
    password: "safegym",
    role: "member",
    created: new Date(),
    gymId: 1
  },
  {
    id: 4,
    firstName: "John",
    lastName: "Davis",
    email: "john@hotmail.com",
    street: "Boulevard General Wahis",
    number: 424,
    zipcode: 1060,
    city: "Saint-Gilles",
    password: "safegym",
    role: "member",
    created: new Date(),
    gymId: 1
  }
];

module.exports = users