const env = {
    database: 'safegym',
    username: 'root',
    password: '',
    host: 'localhost',
    dialect: 'mysql',
    dialectOptions: {
        timezone: '+01:00',
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    logging: false //console.log
  };
   
  module.exports = env;