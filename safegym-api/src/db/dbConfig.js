const Sequelize = require('sequelize');
const env = require('./env.js');

const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  dialect: env.dialect,
  dialectOptions: {
    timezone: env.dialectOptions.timezone,
  },
  pool: {
    max: env.pool.max,
    min: env.pool.min,
    acquire: env.pool.acquire,
    idle: env.pool.idle,
  },
  logging: env.logging
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;


db.gyms = require('../models/gym.js')(sequelize, Sequelize);
db.users = require('../models/user.js')(sequelize, Sequelize);
db.rdvs = require('../models/rdv.js')(sequelize, Sequelize);
db.trainings = require('../models/training.js')(sequelize, Sequelize);
db.exercises = require('../models/exercise.js')(sequelize, Sequelize);
db.meals = require('../models/meal.js')(sequelize, Sequelize);
db.foods = require('../models/food.js')(sequelize, Sequelize);


//Gym hasMany Users (1 -> *)
db.gyms.hasMany(db.users, {foreignKey: 'gymId', sourceKey: 'id', onDelete: 'cascade'});
db.users.belongsTo(db.gyms, {foreignKey: 'gymId', targetKey: 'id'});

//User hasMany Rdvs (1 -> *)
db.users.hasMany(db.rdvs, {foreignKey: 'userId', sourceKey: 'id', onDelete: 'cascade'});
db.rdvs.belongsTo(db.users, {foreignKey: 'userId', targetKey: 'id'});

//User hasMany Training (1 -> *)
db.users.hasMany(db.trainings, {foreignKey: 'userId', sourceKey: 'id', onDelete: 'cascade'});
db.trainings.belongsTo(db.users, {foreignKey: 'userId', targetKey: 'id'});

//Training hasMany Exercise (1 -> *)
db.trainings.hasMany(db.exercises, {foreignKey: 'trainingId', sourceKey: 'id', onDelete: 'cascade'});
db.exercises.belongsTo(db.trainings, {foreignKey: 'trainingId', targetKey: 'id'});

//User hasMany Meals (1 -> *)
db.users.hasMany(db.meals, {foreignKey: 'userId', sourceKey: 'id', onDelete: 'cascade'});
db.meals.belongsTo(db.users, {foreignKey: 'userId', targetKey: 'id'});

//Meal hasMany Food (1 -> *)
db.meals.hasMany(db.foods, {foreignKey: 'mealId', sourceKey: 'id', onDelete: 'cascade'});
db.foods.belongsTo(db.meals, {foreignKey: 'mealId', targetKey: 'id'});



module.exports = db;