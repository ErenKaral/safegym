const gyms = [
  {
    id: 1,
    name: "Safe GYM Brussels",
    address: "Av. de l'Astronomie 19, 1210 Saint-Josse-ten-Noode",
    maxPlace : 3,
    created: new Date()
  }
];

module.exports = gyms