module.exports = (sequelize, DataTypes) => {
  return sequelize.define('Meal', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    date: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }, {
    timestamps: true,
    createdAt: false,
    updatedAt: false
  })
}