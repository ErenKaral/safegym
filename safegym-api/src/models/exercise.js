module.exports = (sequelize, DataTypes) => {
  return sequelize.define('Exercise', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    set: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    repetition: {
      type: DataTypes.INTEGER,
      allowNull: true,
    }
  }, {
    createdAt: false,
    updatedAt: false
  })
}