module.exports = (sequelize, DataTypes) => {
return sequelize.define('Training', {
	id: {
		type: DataTypes.INTEGER,
		allowNull: false,
		primaryKey: true,
		autoIncrement: true
	},
	name: {
		type: DataTypes.STRING,
		allowNull: false
	},
	date: {
		type: DataTypes.DATE,
		allowNull: false
	},
	duration: {
		type: DataTypes.INTEGER,
		allowNull: false
	},
}, {
	timestamps: true,
	createdAt: false,
	updatedAt: false
})
}