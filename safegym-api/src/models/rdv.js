module.exports = (sequelize, DataTypes) => {
  return sequelize.define('Rdv', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    duration: {
      type: DataTypes.INTEGER,
      allowNull: false,
    }
  }, {
    timestamps: true,
    createdAt: 'created',
    updatedAt: false
  })
}

  