const nodemailer = require("nodemailer");
const { google } = require('googleapis')
const config = require('./config')
const OAuth2 = google.auth.OAuth2

const OAuth2_client = new OAuth2(config.clientId, config.clientSecret)
OAuth2_client.setCredentials( { refresh_token : config.refreshToken } )

module.exports = {

  sendSignupMail: function(name, recipient, password) {
    const accessToken = OAuth2_client.getAccessToken()
  
    const transport = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        type: 'OAuth2',
        user: config.user,
        clientId: config.clientId,
        clientSecret: config.clientSecret,
        refreshToken: config.refreshToken,
        accessToken: accessToken
      }
    })
    
    const mail_options = {
      from: `Safe GYM <${config.user}>`,
      to: recipient,
      subject: 'Bienvenue sur Safe GYM !',
      html: get_html_message(name, recipient, password)
    }
  
  
    transport.sendMail(mail_options, function(error, result) {
      if (error) {
        console.log('Error: ', error)
      } else {
        console.log('Success: ', result)
      }
      transport.close()
    })
  
  }

}
  
function get_html_message(name, recipient, password) {
  return `
  <h1>Bienvenue sur Safe GYM, ${name} ! &#129321;</h1>
  <p>Nous vous remercions de votre intérêt à Safe GYM.</p>
  <p>Votre adresse e-mail: <strong>${recipient}</strong></p>
  <p>Votre mot de passe: <strong>${password}</strong></p>
  <p>Vous pouvez vous connecter à tout moment sur notre site avec ces indentifiants.</p>
  <br>
  <p>Nous vous souhaitons un bon entraînement !</p>
  <br><hr>
  <h3>&#127947; L'équipe Safe GYM. &#127947;</h3>
  `
}
  