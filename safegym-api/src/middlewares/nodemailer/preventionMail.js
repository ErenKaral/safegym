const nodemailer = require("nodemailer");
const { google } = require('googleapis')
const config = require('./config')
const OAuth2 = google.auth.OAuth2

const OAuth2_client = new OAuth2(config.clientId, config.clientSecret)
OAuth2_client.setCredentials( { refresh_token : config.refreshToken } )

module.exports = {

  sendPreventionMail: function(recipient, date) {
    const accessToken = OAuth2_client.getAccessToken()
  
    const transport = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        type: 'OAuth2',
        user: config.user,
        clientId: config.clientId,
        clientSecret: config.clientSecret,
        refreshToken: config.refreshToken,
        accessToken: accessToken
      }
    })
    
    const mail_options = {
      from: `Safe GYM <${config.user}>`,
      to: recipient,
      subject: 'Attention, IMPORTANT !',
      html: get_html_message(date)
    }
  
    transport.sendMail(mail_options, function(error, result) {
      if (error) {
        console.log('Error: ', error)
      } else {
        console.log('Success: ', result)
      }
      transport.close()
    })
  
  }

}

function formatDate (date) {
  if (!date) return true;
  const [year, month, day] = date.split('-');
  return `${day}/${month}/${year}`;
}
  
function get_html_message(date) {
  return `
  <h3>⚠️ Votre attention s'il vous plaît !</h3>
  <p>Il semblerait que vous vous êtes entraîné chez nous le <strong>${formatDate(date.substring(0,10))}</strong>.</p>
  <p>D'après les informations que nous avons reçus, une personne testée positive du <strong>COVID-19</strong>
  s'est entraînée le même jour que vous...</p>
  <p><strong>Nous vous recommandons vivement de vous faire tester au plus vite !</strong></p>
  <br>
  <p>À bientôt, en forme ! &#128170;</p>
  <br><hr>
  <h3>&#127947; L'équipe Safe GYM. &#127947;</h3>
  `
}
  