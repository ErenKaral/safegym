const nodemailer = require("nodemailer");
const { google } = require('googleapis')
const config = require('./config')
const OAuth2 = google.auth.OAuth2

const OAuth2_client = new OAuth2(config.clientId, config.clientSecret)
OAuth2_client.setCredentials( { refresh_token : config.refreshToken } )

module.exports = {

  sendConfirmMail: function(name, recipient, date, duration) {
    const accessToken = OAuth2_client.getAccessToken()
  
    const transport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            type: 'OAuth2',
            user: config.user,
            clientId: config.clientId,
            clientSecret: config.clientSecret,
            refreshToken: config.refreshToken,
            accessToken: accessToken
        }
    })
    
    const mail_options = {
        from: `Safe GYM <${config.user}>`,
        to: recipient,
        subject: 'Confirmation de rendez-vous.',
        html: get_html_message(name, date, duration)
    }
  
  
    transport.sendMail(mail_options, function(error, result) {
        if (error) {
            console.log('Error: ', error)
        } else {
            console.log('Success: ', result)
        }
        transport.close()
    })
  
  }

}

function formatDate (date) {
  if (!date) return true;
  const [year, month, day] = date.split('-');
  return `${day}/${month}/${year}`;
}
  
function get_html_message(name, date, duration) {
  return `
  <h1>Bonjour ${name}, votre rendez-vous est confirmé ! &#10004;</h1>
  <p>Nous sommes impatients de vous voir au club le <strong>${formatDate(date.substring(0,10))}</strong> 
  à partir de <strong>${date.substring(11,16)}</strong>.</p>
  <p>Vous aurez accès au club pour une durée de <strong>${duration}</strong> minutes.</p>
  <p>Après cela votre, réservation expire et vous ne pourrez pas entrer dans la salle.</p>
  <p>Veillez à respecter la distanciation sociale de <strong>1,5 m</strong> pendant votre entraînement.</p>
  <br>
  <p>Nous vous souhaitons d'avance un bon entraînement !</p>
  <br><hr>
  <h3>&#127947; L'équipe Safe GYM. &#127947;</h3>
  `
}
  