const { check, validationResult } = require('express-validator');
const User = require('../../controllers/userController');

exports.validateUser = [
	check('firstName')
    .trim()
    .isLength({ min: 2, max: 20 })
    .withMessage('Le prénom doit être entre 2 et 20 caractères de long.'),
  check('lastName')
    .trim()
    .isLength({ min: 2, max: 20 })
    .withMessage('Le nom doit être entre 2 et 20 caractères de long.'),
  check('email')
    .trim()
    .isEmail()
    .withMessage('E-mail invalide.')
    .normalizeEmail()
    .toLowerCase()
    .custom(email => {
      return User.findUserByEmail(email)
        .then(user => {
          if (user) { return Promise.reject(`L'email est déja utilisé par un autre utilisateur.`); }
        })
    }),
  check('street')
    .trim()
    .isLength({ min: 2, max: 30 })
    .withMessage('La rue doit être entre 2 et 30 caractères de long.'),
  check('number', 'Le nombre doit être entre 1 et 999.')
    .isFloat({ min: 1, max: 999 }),
  check('zipcode', 'Le code postale doit être plus grand que 0.')
    .isFloat({ min: 1 }),
  check('city')
    .trim()
    .isLength({ min: 2, max: 30 })
    .withMessage('La ville doit être entre 2 et 30 caractères de long.'),
  check('password')
    .trim()
    .isLength({ min: 6 })
    .withMessage('Le mot de passe doit contenir au moins 6 caractères.'),
	
	(req, res, next) => {
		const errors = validationResult(req)
		if (!errors.isEmpty()) {
			return res.status(400).json({
					success: false,
					errors: errors.array()
			});
		}
		next();
	},
];