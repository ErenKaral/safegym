const { check, validationResult } = require('express-validator');

exports.validateTraining = [
	check('name')
    .trim()
    .isLength({ min: 2, max: 30 })
    .withMessage('Le nom doit être entre 2 et 30 caractères de long.'),
  check('duration', 'La durée doit être entre 30 et 120 min')
    .isFloat({ min: 30, max: 120 }),

	(req, res, next) => {
		const errors = validationResult(req)
		if (!errors.isEmpty()) {
			return res.status(400).json({
        success: false,
        errors: errors.array()
			});
		}
		next();
	},
];