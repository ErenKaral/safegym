const { check, validationResult } = require('express-validator');

exports.validateFood = [
	check('name')
    .trim()
    .isLength({ min: 2 })
    .withMessage('Le nom doit être au moins 2 caractères de long.'),
  check('calories', 'Le nombre de calories doit être entre 0 et 1500.')
    .isFloat({ min: 0, max: 1500 }),
  check('fats', 'Le nombre de lipides doit être entre 0 et 100.')
    .isFloat({ min: 0, max: 100 }),
  check('carbs', 'Le nombre de glucides doit être entre 0 et 200.')
    .isFloat({ min: 0, max: 200 }),
  check('proteins', 'Le nombre de protéines doit être entre 0 et 100.')
    .isFloat({ min: 0, max: 100 }),

	(req, res, next) => {
		const errors = validationResult(req)
		if (!errors.isEmpty()) {
			return res.status(400).json({
        success: false,
        errors: errors.array()
			});
		}
		next();
	},
];