const { check, validationResult } = require('express-validator');

exports.validateMeal = [
	check('name')
    .trim()
    .isLength({ min: 2 })
    .withMessage('Le nom doit être au moins 2 caractères de long.'),

	(req, res, next) => {
		const errors = validationResult(req)
		if (!errors.isEmpty()) {
			return res.status(400).json({
        success: false,
        errors: errors.array()
			});
		}
		next();
	},
];