const { check, validationResult } = require('express-validator');

exports.validateGym = [
	check('name')
    .trim()
    .isLength({ min: 2 })
    .withMessage('Le nom doit être au moins 2 caractères de long.'),
  check('address')
    .trim()
    .isLength({ min: 2 })
    .withMessage(`L'adresse doit être au moins 2 caractères de long.`),
  check('maxPlace', 'Le nombre minimum doit être valide')
    .isFloat({ min: 0 }),

	(req, res, next) => {
		const errors = validationResult(req)
		if (!errors.isEmpty()) {
			return res.status(400).json({
        success: false,
        errors: errors.array()
			});
		}
		next();
	},
];