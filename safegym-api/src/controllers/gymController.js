const db = require('../db/dbConfig');

const Gym = db.gyms;

exports.getGym = (req, res) => {
  Gym.findByPk(req.params.id)
    .then(gym => {
      if(gym === null) {
        const message = `La salle demandée n'existe pas. Réessayez avec un autre identifiant.`
        return res.status(404).json({ message })
      }
      const message = 'La salle a bien été trouvée.'
      res.json({ message, data: gym })
    })
    .catch(error => {
      const message = `La salle n'a pas pu être récupérée. Réessayez dans quelques instants.`
      res.status(500).json({ message, data: error })
    })
}

// Update the gym with the given gym id.
exports.updateGym = (req, res) => {
  const id = req.params.id
  Gym.update(req.body, { where: { id: id} })
  .then(_ => {
    Gym.findByPk(id).then(gym => {
      const message = `La salle ${gym.name} a bien été modifiée.`
      res.json({message, data: gym })
    })
  })
};
