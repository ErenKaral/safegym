const db = require('../db/dbConfig');

const Food = db.foods;
const Meal = db.meals;

//Create a new food by the given meal id.
exports.create = (req, res) => {
  Meal.findByPk(req.params.id)
  .then(meal => {
    if(meal === null) {
      const message = `Ce repas n'existe pas. Réessayez avec un autre identifiant.`
      return res.status(404).json({ message })
    }
    Food.create({
      name: req.body.name,
      calories: req.body.calories,
      fats: req.body.fats,
      carbs: req.body.carbs,
      proteins: req.body.proteins,
      mealId: req.params.id
    })
    .then(food => {
      const message = `L'aliment ${req.body.name} a bien été ajouté.`
      res.json({ message, data: food })
    })
  })
  .catch(error => {
    const message = `L'exercice n'a pas pu être ajouté. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


// Delete the food(s) with the given meal id.
exports.deleteFood = (req, res) => {
  Food.findByPk(req.params.id)
  .then(food => {
    if(food === null) {
      const message = `Cet aliment n'existe pas. Réessayez avec un autre identifiant.`
      return res.status(404).json({ message })
    }
    return Food.destroy({ where: { id: food.id } })
    .then(_ => {
      const message = `L'aliment ${food.name} a bien été supprimé.`
      res.json({ message, data: food })
    })
  })
  .catch(error => {
    const message = `L'aliment n'a pas pu être supprimé. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


//Update the food with the given id.
exports.updateFood = (req, res) => {
  Food.findByPk(req.params.id)
  .then(food => {
    if(food === null) {
      const message = `L'aliment n'existe pas. Réessayez avec un autre identifiant.`
      return res.status(404).json({ message })
    }
    Food.update(req.body, { where: { id: req.params.id} })
    .then(_ => {
      Food.findByPk(req.params.id).then(food => {
        const message = `L'aliment ${food.name} a bien été modifié.`
        res.json({ message, data: food})
      })
    })
  })
  .catch(error => {
    const message = `Le repas n'a pas pu être modifié. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};
