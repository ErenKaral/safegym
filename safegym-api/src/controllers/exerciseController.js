const db = require('../db/dbConfig');

const Training = db.trainings;
const Exercise = db.exercises;


//Create a new exercise for the given training by id.
exports.create = (req, res) => {
  Training.findByPk(req.params.id)
  .then(training => {
    if(training === null) {
      const message = `Cet entraînement n'existe pas. Réessayez avec un autre identifiant.`
      return res.status(404).json({ message })
    }
    Exercise.create({
      name: req.body.name,
      set: req.body.set,
      repetition: req.body.repetition,
      trainingId: req.params.id,
    })
    .then(exercise => {
      const message = `L'exercice ${req.body.name} a bien été créé.`
      res.json({ message, data: exercise })
    })
  })
  .catch(error => {
    const message = `L'exercice n'a pas pu être ajouté. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};

// Delete the exercise(s) with the given training id.
exports.deleteExercise = (req, res) => {
  Exercise.findByPk(req.params.id)
  .then(exercise => {
    if(exercise === null) {
      const message = `Cet exercice n'existe pas. Réessayez avec un autre identifiant.`
      return res.status(404).json({ message })
    }
    return Exercise.destroy({ where: { id: exercise.id } })
    .then(_ => {
      const message = `L'exercice ${exercise.name} a bien été supprimé.`
      res.json({ message, data: exercise })
    })
  })
  .catch(error => {
    const message = `L'exercice n'a pas pu être supprimé. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


//Update the exercise with the given id.
exports.updateExercise = (req, res) => {
  Exercise.findByPk(req.params.id)
  .then(exercise => {
    if(exercise === null) {
      const message = `Cet exercice n'existe pas. Réessayez avec un autre identifiant.`
      return res.status(404).json({ message })
    }
    Exercise.update(req.body, { where: { id: req.params.id} })
    .then(_ => {
      Exercise.findByPk(req.params.id).then(exercise => {
        const message = `L'exercice ${exercise.name} a bien été modifié.`
        res.json({ message, data: exercise})
      })
    })
  })
  .catch(error => {
    const message = `L'exercice n'a pas pu être modifié. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};
  