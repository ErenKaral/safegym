const { Op } = require("sequelize");
const bcrypt = require('bcrypt');
const db = require('../db/dbConfig');
const jwt = require('jsonwebtoken');
const privateKey = require('../auth/private_key');
const mail = require('../middlewares/nodemailer/signupMail');
const customMail = require('../middlewares/nodemailer/customMail');
const preventionMail = require('../middlewares/nodemailer/preventionMail')

//Collections:
const User = db.users;
const Rdv = db.rdvs;


//Find user by mail and login with generating a JWT token.
exports.login = (req, res) => {
	User.findOne({ where: { email: req.body.email } })
	.then(user =>{
		if(!user){
			const message = `L'utilisateur demandé n'existe pas.`;
			return res.status(404).json({ message })
    }
		
    return bcrypt.compare(req.body.password, user.password).then(isPasswordValid => {
  
      if(!isPasswordValid) {
        const message = `Le mot de passe est incorrect.`;
        return res.status(401).json({ message })
      }
  
      //Générer un jeton JWT valide pendant 24 heures.
      const token = jwt.sign(
        { userId: user.id },
        privateKey,
        { expiresIn: '24h' }
      );
  
      const message = `L'utilisateur ${req.body.email} a été connecté avec succès.`;
      return res.json({ message, data: user, token })
    })
  })
  .catch(error => {
    const message = `L'utilisateur n'a pas pu être connecté. Réessayez dans quelques instants.`;
    res.status(500).json({ message, data: error })
  })
};


// Signup the user.
exports.signup = (req, res) => {
  bcrypt.hash(req.body.password, 10)
  .then(hash => {
    User.create({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      street: req.body.street,
      number: req.body.number,
      zipcode: req.body.zipcode,
      city: req.body.city,
      password: hash,
      role: req.body.role,
      gymId: req.body.gymId
    })
    .then(user => {
      //Générer un jeton JWT valide pendant 24 heures.
      const token = jwt.sign(
        { userId: user.id },
        privateKey,
        { expiresIn: '24h' }
      );
      const message = `L'utilisateur ${req.body.firstName} ${req.body.lastName} a bien été créé.
                      Un mail de confirmation est envoyé à ${req.body.email}.`
      res.json({ message, data: user, token })

      mail.sendSignupMail(`${req.body.firstName}`, `${req.body.email}`, `${req.body.password}`) //Send welcome mail to the user.
    }) 
    .catch(error => {
      const message = `L'utilisateur n'a pas pu être ajouté. Réessayez dans quelques instants.`
      res.status(500).json({ message, data: error })
    })
  });
};


// Return the user with the given id.
exports.findByPk = async (req, res) => {
  User.findByPk(req.params.id)
    .then(user => {
      if(user === null) {
        const message = `L'utilisateur demandé n'existe pas. Réessayez avec un autre identifiant.`
        return res.status(404).json({ message })
      }
      const message = 'Un utilisateur a bien été trouvé.'
      res.json({ message, data: user })
    })
    .catch(error => {
      const message = `L'utilisateur n'a pas pu être récupéré. Réessayez dans quelques instants.`
      res.status(500).json({ message, data: error })
    })
};


// Return all users
exports.findUsers = (req, res) => {
  User.findAll({
    attributes: ['id', 'firstName', 'lastName', 'email', 'street', 'number', 'zipcode', 'city', 'password', 'role', 'created'],
  })
  .then(users => {
    const message = 'La liste des utilisateurs a bien été récupérée.'
    res.json({ message, data: users })
  })
  .catch(error => {
    const message = `La liste des utilisateurs n'a pas pu être récupérée. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


//Return all users with the given Rdv date.
exports.findUsersByDate = (req, res) => {
  User.findAll({
    attributes: ['id', 'firstName', 'lastName', 'email', 'password', 'role', 'created'],
    include: [{
      model: Rdv,
      where: { userId: db.Sequelize.col('userId'), date: { [Op.substring]: req.params.date.substring(0,10) } },
      attributes: ['id', 'date', 'duration', 'created', 'userId']
    }]
  })
  .then(users => {
    const message = 'La liste des utilisateurs a bien été récupérée.'
    res.json({ message, data: users })
  })
  .catch(error => {
    const message = `La liste n'a pas pu être récupérée. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


// Delete the user with the given id.
exports.deleteUser = (req, res) => {
  User.findByPk(req.params.id)
  .then(user => {
    if(user === null) {
      const message = `L'utilisateur demandé n'existe pas. Réessayez avec un autre identifiant.`
      return res.status(404).json({ message })
    }

    return User.destroy({ where: { id: user.id } })
    .then(_ => {
      const message = `L'utilisateur ${user.firstName} ${user.lastName} a bien été supprimé.`
      res.json({message, data: user })
    })
  })
  .catch(error => {
    const message = `L'utilisateur n'a pas pu être supprimé. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


// Update the user with the given id.
exports.updateUser = (req, res) => {
  const id = req.params.id
    User.findByPk(id)
    .then(user => {
      if(user === null) {
        const message = `L'utilisateur demandé n'existe pas. Réessayez avec un autre identifiant.`
        return res.status(404).json({ message })
      }
      user.firstName = req.body.firstName;
      user.lastName = req.body.lastName;
      user.email = req.body.email;
      user.street = req.body.street;
      user.number = req.body.number;
      user.zipcode = req.body.zipcode;
      user.city = req.body.city;
      user.role = req.body.role;
      user.save().then(_ => {
        const message = `L'utilisateur ${user.firstName} ${user.lastName} a bien été modifié.`
        res.json({message, data: user })
      })
    })
    .catch(error => {
      const message = `L'utilisateur n'a pas pu être modifié. Réessayez dans quelques instants.`
      res.status(500).json({ message, data: error })
    })
};


//Send prevention mail.
exports.sendPreventionMail = (req, res) => {
  preventionMail.sendPreventionMail(`${req.body.emails}`, `${req.body.date}`)
  const message = `Les membres ont été prévenus par mail.`
  res.json({ message })
}


//Send custom mail.
exports.sendCustomMail = (req, res) => {
  customMail.sendCustomMail(`${req.body.emails}`, `${req.body.subject}`, `${req.body.body}`)
  const message = `Le mail a bien été envoyé`
  res.json({ message })
}


// Return the user with the given email
exports.findUserByEmail = (email) => {
  return User.findOne({ where: { email: email } })
};


