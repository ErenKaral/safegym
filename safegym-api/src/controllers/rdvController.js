const { Op } = require("sequelize");
const db = require('../db/dbConfig');
const mail = require('../middlewares/nodemailer/rdvConfirmMail');

//Collections:
const Rdv = db.rdvs;

//Create a new rdv for the given user by id.
exports.create = (req, res) => {
  Rdv.findAll({ 
    where: { userId: req.body.userId, date: { [Op.substring]: req.body.date.substring(0,10) } }
  })
  .then(rdv => {
    if(rdv.length > 0) {
      const message = `Vous avez déja un rendez-vous pour ce jour.`
      return res.status(404).json({ message })
    } 
      Rdv.create({
        date: req.body.date,
        duration: req.body.duration,
        userId: req.body.userId,
      })
      .then(rdv => {
        const message = `Le rendez-vous ${req.body.date} a bien été crée.`
        mail.sendConfirmMail(`${req.body.firstName}`, `${req.body.email}`, `${req.body.date}`, `${req.body.duration}`)
        return res.json({ message, data: rdv })
      })
  })
  .catch(error => {
    const message = `Le rendez-vous n'a pas pu être ajouté. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


// Return rdv(s) by the given user id.
exports.findAllByUserPk = (req, res) => {
  Rdv.findAll({ 
    where: { userId: req.params.id } 
  })
  .then(rdv => {
    if(rdv === null) {
      const message = `L'utilisateur demandé n'existe pas. Réessayez avec un autre identifiant.`
      return res.status(404).json({ message })
    }
    const message = 'Rendez-vous trouvé.'
    res.json({ message, data: rdv })
  })
  .catch(error => {
    const message = `L'utilisateur n'a pas pu être récupéré. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


// Return all rdv(s) by the given date.
exports.findAllByDate = (req, res) => {
  Rdv.findAll({ 
    where: { date: { [Op.substring]: req.params.date.substring(0,10) } } 
  })
  .then(rdvs => {
    const message = 'La liste des rendez-vous a été récupérée'
    res.json({ message, data: rdvs })
  })
  .catch(error => {
    const message = `La liste n'a pas pu être récupérée. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
}


// Delete the user with the given id.
exports.deleteRdv = (req, res) => {
  Rdv.findByPk(req.params.id)
  .then(rdv => {
    if(rdv === null) {
      const message = `Ce rendez-vous n'existe pas. Réessayez avec un autre identifiant.`
      return res.status(404).json({ message })
    }

    return Rdv.destroy({ where: { id: rdv.id } })
    .then(_ => {
      const message = `Le rendez-vous avec l'identifiant n°${rdv.id} a bien été supprimé.`
      res.json({message, data: rdv })
    })
  })
  .catch(error => {
    const message = `Le rendez-vous n'a pas pu être supprimé. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


