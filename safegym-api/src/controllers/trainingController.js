const { Op } = require("sequelize");
const db = require('../db/dbConfig');

const Training = db.trainings;
const Exercise = db.exercises;

//Create a new training for the given user by id.
exports.create = (req, res) => {
  Training.create({
    name: req.body.name,
    date: req.body.date,
    duration: req.body.duration,
    userId: req.body.userId,
    Exercises: req.body.exercises //Training contains one or more exercise(s).
  }, { 
    include: [ Exercise ]
  }).then(training => {
    const message = `L'entraînement ${req.body.name} a bien été créé.`
    res.json({ message, data: training })
  })
  .catch(error => {
    const message = `L'entraînement n'a pas pu être ajouté. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


// Return all trainings include exercises by the given date & user id.
exports.findAllByDate = (req, res) => {
  Training.findAll({
    attributes: ['id', 'name', 'date', 'duration', 'userId'],
    where: { userId: req.params.id, date: { [Op.substring]: req.params.date.substring(0,10) } },
    include: [{
      model: Exercise,
      where: { id: db.Sequelize.col('trainingId') },
      attributes: ['id', 'name', 'set', 'repetition', 'trainingId']
    }],
  })
  .then(trainings => {
    const message = 'La liste des entraînements a bien été récupérée.'
    res.json({ message, data: trainings })
  })
  .catch(error => {
    const message = `La liste des entraînements n'a pas pu être récupérée. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


// Return all trainings include exercises by the given user id.
exports.findAllByUserPk = (req, res) => {
  Training.findAll({
    attributes: ['id', 'name', 'date', 'duration', 'userId'],
    where: { userId: req.params.id },
    include: [{
      model: Exercise,
      where: { id: db.Sequelize.col('trainingId') },
      attributes: ['id', 'name', 'set', 'repetition', 'trainingId']
    }]
  })
  .then(trainings => {
    const message = 'La liste des entraînements a bien été récupérée.'
    res.json({ message, data: trainings })
  })
  .catch(error => {
    const message = `La liste n'a pas pu être récupérée. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


// Delete the training and exercise(s) with the given id.
exports.deleteTraining = (req, res) => {
  Training.findByPk(req.params.id)
  .then(training => {
    if(training === null) {
      const message = `Cet entraînement n'existe pas. Réessayez avec un autre identifiant.`
      return res.status(404).json({ message })
    }

    return Training.destroy({ where: { id: training.id } })
    .then(_ => {
      const message = `L'entraînement ${training.name} a bien été supprimé.`
      res.json({ message, data: training })
    })
  })
  .catch(error => {
    const message = `L'entraînement n'a pas pu être supprimé. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


//Update the training with the given training id.
exports.updateTraining = (req, res) => {
  Training.findByPk(req.params.id)
  .then(training => {
    if(training === null) {
      const message = `Cet entraînement n'existe pas. Réessayez avec un autre identifiant.`
      return res.status(404).json({ message })
    }
    Training.update(req.body, { where: { id: req.params.id} })
    .then(_ => {
      Training.findByPk(req.params.id).then(training => {
        const message = `L'entraînement ${training.name} a bien été modifié.`
        res.json({ message, data: training})
      })
    })
  })
  .catch(error => {
    const message = `Le repas n'a pas pu être modifié. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};