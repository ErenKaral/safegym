const { Op } = require("sequelize");
const db = require('../db/dbConfig');

const Meal = db.meals;
const Food = db.foods;


//Create a new meal for the given user id.
exports.create = (req, res) => {
  Meal.create({
    name: req.body.name,
    date: req.body.date,
    userId: req.body.userId,
    Food: req.body.food //Meal contains one or more food(s).
  }, { 
    include: [ Food ]
  }).then(meal => {
    const message = `Le repas ${req.body.name} a bien été créé.`
    res.json({ message, data: meal })
  })
  .catch(error => {
    const message = `Le repas n'a pas pu être ajouté. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


// Return all meals include foods by the given date & user id.
exports.findAllByDate = (req, res) => {
  Meal.findAll({
    attributes: ['id', 'name', 'date', 'userId'],
    where: { userId: req.params.id, date: { [Op.substring]: req.params.date.substring(0,10) } },
    include: [{
      model: Food,
      where: { id: db.Sequelize.col('mealId') },
      attributes: ['id', 'name', 'calories', 'fats', 'carbs', 'proteins', 'mealId']
    }],
    order: [['date']]
  })
  .then(meals => {
    const message = 'La liste des repas a bien été récupérée.'
    res.json({ message, data: meals })
  })
  .catch(error => {
    const message = `La liste des repas n'a pas pu être récupérée. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


// Return all meals include foods by the user id.
exports.findAllById = (req, res) => {
  Meal.findAll({
    attributes: ['id', 'name', 'date', 'userId'],
    where: { userId: req.params.id },
    include: [{
      model: Food,
      where: { id: db.Sequelize.col('mealId') },
      attributes: ['id', 'name', 'calories', 'fats', 'carbs', 'proteins', 'mealId']
    }],
  })
  .then(meals => {
    const message = 'La liste des repas a bien été récupérée.'
    res.json({ message, data: meals })
  })
  .catch(error => {
    const message = `La liste des repas n'a pas pu être récupérée. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


// Delete the meal and food(s) with the given id.
exports.deleteMeal = (req, res) => {
  Meal.findByPk(req.params.id)
  .then(meal => {
    if(meal === null) {
      const message = `Ce repas n'existe pas. Réessayez avec un autre identifiant.`
      return res.status(404).json({ message })
    }
    return Meal.destroy({ where: { id: meal.id } })
    .then(_ => {
      const message = `Le repas ${meal.name} a bien été supprimé.`
      res.json({ message, data: meal })
    })
  })
  .catch(error => {
    const message = `L'entraînement n'a pas pu être supprimé. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};


//Update the meal with the given meal id.
exports.updateMeal = (req, res) => {
  Meal.findByPk(req.params.id)
  .then(meal => {
    if(meal === null) {
      const message = `Ce repas n'existe pas. Réessayez avec un autre identifiant.`
      return res.status(404).json({ message })
    }
    Meal.update(req.body, { where: { id: req.params.id} })
    .then(_ => {
      Meal.findByPk(req.params.id).then(meal => {
        const message = `Le repas ${meal.name}  bien été modifié.`
        res.json({ message, data: meal})
      })
    })
  })
  .catch(error => {
    const message = `Le repas n'a pas pu être modifié. Réessayez dans quelques instants.`
    res.status(500).json({ message, data: error })
  })
};